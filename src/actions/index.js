import { Client } from 'maximesignoret_sdk';
import {
  REQUEST_LINES,
  RECEIVE_LINES,
  SELECT_CATEGORY,
  INVALIDATE_CATEGORY,
  REQUEST_RESUME,
  RECEIVE_RESUME
} from '../constants';

const client = new Client({
  url: process.env.REACT_APP_API_URI,
  port: process.env.REACT_APP_API_PORT,
  entrypoint: process.env.REACT_APP_API_ENTRYPOINT,
  staticFiles: process.env.REACT_APP_API_STATIC
});

export const selectCategory = category => ({
  type: SELECT_CATEGORY,
  category
});

export const invalidateCategory = category => ({
  type: INVALIDATE_CATEGORY,
  category
});

export const requestLines = category => ({
  type: REQUEST_LINES,
  category
});

export const requestResume = () => ({
  type: REQUEST_RESUME,
  category: 'resume'
});

export const receiveLines = (category, lines) => ({
  type: RECEIVE_LINES,
  category,
  lines,
  receivedAt: Date.now()
});

export const receiveResume = (category, lines) => ({
  type: RECEIVE_RESUME,
  category,
  lines,
  receivedAt: Date.now()
});

const getResume = category => dispatch => {
  dispatch(requestResume());
  return client
    .request({
      path: `/${category}`
    })
    .then(response => dispatch(receiveResume(category, response)))
    .catch(() => console.log('CATCH RESUME'));
};

const fetchLines = category => dispatch => {
  dispatch(requestLines(category));
  return client
    .request({
      path: `/${category.replace(/([A-Z])/g, '-$1')}`
    })
    .then(response => dispatch(receiveLines(category, response[category])))
    .catch(() => dispatch(receiveLines(category, [])));
};

const shouldFetchLines = (state, category) => {
  const lines = state.linesByCategory[category];
  if (!lines) {
    return true;
  }
  if (lines.isFetching) {
    return false;
  }
  return lines.didInvalidate;
};

export const fetchLinesIfNeeded = category => (dispatch, getState) => {
  if (shouldFetchLines(getState(), category)) {
    return dispatch(fetchLines(category));
  }
};

export const fetchResume = category => dispatch =>
  dispatch(getResume(category));
