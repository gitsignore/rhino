import React from 'react';
import { NavLink } from 'react-router-dom';

const Navbar = ({ logo, alt }) => (
  <div className="navbar">
    <div className="navbar-section">
      <img src={logo} alt={alt} style={{ width: 50 }} />
      <h2>Rhino</h2>
    </div>
    <div className="navbar-section">
      <NavLink className="btn btn-link" activeClassName="active" to="/sign-in">
        Sign in
      </NavLink>
      <NavLink className="btn btn-link" activeClassName="active" to="/sign-in">
        Sign up
      </NavLink>
      <NavLink className="btn btn-link" activeClassName="active" to="/sign-out">
        Sign out
      </NavLink>
    </div>
  </div>
);

export default Navbar;
