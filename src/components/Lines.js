import React from 'react';
import PropTypes from 'prop-types';

const Lines = ({ lines }) => (
  <ul>{lines.map((line, i) => <li key={i}>{line.title}</li>)}</ul>
);

Lines.propTypes = {
  lines: PropTypes.array.isRequired
};

export default Lines;
