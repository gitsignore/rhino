import React from 'react';

const Login = ({ logo }) => (
  <div className="column col-4 col-xs-4 col-mx-auto">
    <div className="panel">
      <div className="panel-header text-center">
        <figure className="avatar avatar-xl">
          <img src={logo} alt="Avatar" />
        </figure>
        <div className="panel-title h5 mt-10">Rhino</div>
        <div className="panel-subtitle">THE HULK</div>
      </div>
      <nav className="panel-nav">
        <ul className="tab tab-block">
          <li className="tab-item active">
            <a href="#panels">Sign In</a>
          </li>
          <li className="tab-item">
            <a href="#panels">Sign Up</a>
          </li>
        </ul>
      </nav>
      <div className="panel-body">
        <div className="tile tile-centered">
          <div className="tile-content">
            <div className="has-icon-right">
              <input
                className="form-input input-lg"
                type="email"
                placeholder="Email"
              />
              <i className="form-icon loading" />
            </div>
          </div>
        </div>
        <div className="tile tile-centered">
          <div className="tile-content">
            <div className="has-icon-right">
              <input
                className="form-input input-lg"
                type="password"
                placeholder="Password"
              />
              <i className="form-icon icon icon-check" />
            </div>
          </div>
        </div>
      </div>
      <div className="panel-footer">
        <button className="btn btn-primary btn-block">Sign In</button>
      </div>
    </div>
  </div>
);

export default Login;
