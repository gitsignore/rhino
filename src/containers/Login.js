import React, { Component } from 'react';
import Login from '../components/Login';
import logo from '../rhino.jpg';

class Home extends Component {
  render() {
    return (
      <div className="container">
        <div className="columns">
          <div className="column col-12">
            <Login logo={logo} alt="rhino" />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
