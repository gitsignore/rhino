import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  selectCategory,
  fetchLinesIfNeeded,
  invalidateCategory
} from '../actions';
import { capitalize } from '../constants';
import Tabs from '../components/Tabs';
import Lines from '../components/Lines';

class List extends Component {
  static propTypes = {
    selectedCategory: PropTypes.string,
    lines: PropTypes.array,
    isFetching: PropTypes.bool.isRequired,
    lastUpdated: PropTypes.number,
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    const { dispatch, selectedCategory } = this.props;
    dispatch(fetchLinesIfNeeded(selectedCategory));
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedCategory !== this.props.selectedCategory) {
      const { dispatch, selectedCategory } = nextProps;
      dispatch(fetchLinesIfNeeded(selectedCategory));
    }
  }

  handleChange = nextCategory => {
    this.props.dispatch(selectCategory(nextCategory));
  };

  handleRefreshClick = e => {
    e.preventDefault();

    const { dispatch, selectedCategory } = this.props;
    dispatch(invalidateCategory(selectedCategory));
    dispatch(fetchLinesIfNeeded(selectedCategory));
  };

  render() {
    const { selectedCategory, lines, isFetching, lastUpdated } = this.props;
    const isEmpty = null === lines || lines.length === 0;
    return (
      <div className="column col-9 col-md-12">
        <h1>{capitalize(selectedCategory)}</h1>
        {lastUpdated && (
          <span className="chip">
            Last updated at {new Date(lastUpdated).toLocaleTimeString()}.{' '}
          </span>
        )}
        <Tabs
          handleRefreshClick={this.handleRefreshClick}
          isFetching={isFetching}
        />
        {isEmpty ? (
          isFetching ? (
            <h2>Loading...</h2>
          ) : (
            <div className="empty">
              <div className="empty-icon">
                <i className="icon icon-stop icon-4x" />
              </div>
              <p className="empty-title h5">You have no data.</p>
              <p className="empty-subtitle">Click the button to refresh.</p>
              <div className="empty-action">
                <button
                  className="btn btn-primary"
                  onClick={this.handleRefreshClick}
                >
                  Try to refresh
                </button>
              </div>
            </div>
          )
        ) : (
          <div style={{ opacity: isFetching ? 0.5 : 1 }}>
            <Lines lines={lines} />
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { linesByCategory, selectedCategory } = state;
  const { isFetching, lastUpdated, items: lines } = linesByCategory[
    selectedCategory
  ] || {
    isFetching: true,
    items: []
  };

  return {
    selectedCategory,
    lines,
    isFetching,
    lastUpdated
  };
};

export default connect(mapStateToProps)(List);
