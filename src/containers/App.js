import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { selectCategory, fetchResume, invalidateCategory } from '../actions';
import { InternalsRoutes } from '../routes';
import Navbar from '../components/Navbar';
import Menu from '../components/Menu';
import logo from '../rhino.jpg';

class App extends Component {
  static propTypes = {
    selectedCategory: PropTypes.string,
    linesByCategory: PropTypes.object,
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.dispatch(fetchResume('resume'));
  }

  handleChange = nextCategory => {
    this.props.dispatch(selectCategory(nextCategory));
  };

  handleRefreshClick = e => {
    e.preventDefault();

    const { dispatch } = this.props;
    dispatch(invalidateCategory('resume'));
    dispatch(fetchResume('resume'));
  };

  render() {
    const { selectedCategory, linesByCategory } = this.props;

    return (
      <main>
        <div className="container">
          <Navbar logo={logo} alt="rhino" />
          <div className="columns">
            <Menu
              handleChange={this.handleChange}
              handleRefresh={this.handleRefreshClick}
              items={linesByCategory}
              value={selectedCategory}
            />
            <InternalsRoutes />
          </div>
        </div>
      </main>
    );
  }
}

const mapStateToProps = state => {
  const { selectedCategory, linesByCategory } = state;

  return {
    selectedCategory,
    linesByCategory
  };
};

export default connect(mapStateToProps)(App);
